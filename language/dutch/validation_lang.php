<?php

$lang['required']         = "Het veld %s is een verplicht veld.";
$lang['isset']            = "Het veld %s moet een waarde hebben.";
$lang['valid_email']    = "Het veld %s moet een geldig emailadres bevatten.";
$lang['valid_url']         = "Het veld %s moet een correcte URL bevatten.";
$lang['valid_ip'] 		= "Het veld %s moet een geldig IP bevatten.";
$lang['min_length']        = "Het veld %s moet tenminste %s tekens bevatten.";
$lang['max_length']        = "Het veld %s mag niet meer dan %s tekens bevatten.";
$lang['exact_length']    = "Het veld %s moet exact %s tekens bevatten.";
$lang['alpha']            = "Het veld %s mag alleen letters bevatten.";
$lang['alpha_numeric']    = "Het veld %s mag alleen letters en cijfers bevatten.";
$lang['alpha_dash']        = "Het veld %s mag alleen letters, cijfers, underscores(_) en strepen(-) bevatten.";
$lang['numeric']        = "Het veld %s mag alleen nummers bevatten.";
$lang['integer']		= "Het veld %s moet een natuurlijk getal bevatten.";
$lang['matches']        = "De waarde van het veld %s is niet gelijk aan de waarde van het veld %s.";

?>